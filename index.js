const express = require("express");

const { PORT = 3002 } = process.env;

const app = express();

app.get("*", (req, res) => {
  res.send("hello world");
});

app.listen(PORT);

console.log(`Running on port ${PORT}`);
